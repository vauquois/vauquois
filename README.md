# Vauquois
`Version: Beta 1.0`

The Vauquois Projects consists of  an immersive experience which leveraged the ICAT Cube, a virtual reality walkthrough of a tunnel recreation, and a 360 degree video documentary to explore the experiences of French, German and American troops and civilians at Vauquois Hill overlooking the French city of Verdun – where the shift from street fighting to trench warfare to tunnel and underground warfare on the Western Front lasting four years destroyed the village of Vauquois, scarring both the landscape and historical consciousness of those who were there.

This repository is Vauquois VR, the virtual reality walkthrough of the tunnel recreation.

# Download

You can download Vauquois VR for Windows [here](https://drive.google.com/file/d/1rnylaRiRKtLCdpeujcOe7BU16jMvbz2o/view?usp=sharing). You'll need SteamVR and a Vive to use this version. Just unzip, and run!

You can get Vauquois VR for Oculus Quest. It is in public beta [here](https://drive.google.com/open?id=1oxBYTn9Kv4fnV5whrvAE_H4U21Pz8gCY). 
Use the [installation guide](https://docs.google.com/document/d/1cWJ8_BdjNfw9LlPmRSZzOQTtzRSj6gp332PDlN2keHg/edit?usp=sharing) to get it on your Quest. Contact aries-dev-g@vt.edu for issues.


# Installation (For Developers)

1. Clone or download this repository. 
2. Install Git LFS. Run ``git lfs install``. Repull or reclone if necessary.
3. Open with Unity. For best results, please use the version found in ``ProjectSettings/ProjectVersion.txt``.

# Documentation

## Branches & Forks

As of 4/8/20, Vauquois VR is broken into three distinct versions

1. The original project, built to run on a desktop with SteamVR and Vive. This is the version on the master branch of [this](https://gitlab.com/historyviz/vauquois) repository.

2. The 'Lite' version, built to run on Oculus Quest. This version draws heavily from the main project, but has some assets removed and other asset and performance changes which support building to Quest. This version lives in a [fork](https://gitlab.com/historyviz/vauquoislite) off of the main project. Contact aries-dev-g@vt.edu for more information.

3. An 'study' version which has all of assets from the original project but adds support for an experiment being run by Todd and Zach. This version lives in the ``Text_Study`` branch of the main repository. Contact aries-dev-g@vt.edu for more information.

The rest of the documentation in this README will refer primarily to the original project as it serves as the main source of truth for Vauquois assets in any of the offshoot projects.

## Folder Structure 
Assets should be organized as follows:
```
Assets
    Images
    Materials
    Models
    Scenes
    ...
    <Any other top level assets>
    Systems
        Calibration
        EndOfTunnel
        Generics
        LandingPage
        Lantern
        OpeningScene
        Player
        SceneControl
        Settings
        ...
        <Any other systems implemented>
```

In general, folders at the top level (inside ```Assets\```) should contain assets that are used by much of the application. Examples would include miscellaneous materials, images, models, and scenes. More specialized groups of assets should have their own folder in the Systems folder. Please try to refrain from using a ```Scripts``` folder at the top level.

## Scenes
The important scenes in the project are:

* ```LandingScene```: The 2D UI displayed on the desktop when the app is launched.
* ```VauquoisOpening```: The 360 photo with narration.
* ```VauquoisTunnel```: The main tunnel experience

## Building & Distribution
There are two main building workflows. The first is for building a standalone desktop application meant to be used with SteamVR and Vive. Additionally, there are brief instructions for building the Quest version.

### Standalone Desktop Application
In Unity, Use ctrl-b to build an exe. Make sure that all three of the required scenes are in the build index in this order: ```LandingScene, VauquoisOpening, VauquoisTunnel```. This exe should be contained in a folder with the same name as the exe. Add the user README (found in the Vauquois Google Drive) to the folder, create a zip file containing just this folder, and upload this zip to one of folders in the Release\Public\ folders on the Vauquois Google Drive.

### Vauquois Lite for Oculus Quest
In Unity, Use ctrl-b to build a signed APK.

Log into aries-dev-g@vt.edu's oculus developer account (VT_ARIES_Developers) and upload the [signed](https://developer.oculus.com/distribute/publish-mobile-app-signing/) APK to one of the release channels. Currently, BETA is what we're working out of.

Note: If the APK large (like the current build), you'll need to use the [Oculus CLI](https://dashboard.oculus.com/tools/cli/) to upload the APK to the release channel.

## Systems
Vauquois uses a number of systems which have been developed over the years. Below is an overview of the main ones.

### Calibration
For information about how the calibration system works, see `Assets\Systems\Calibration\Calibration README.md`. Generally, the system is broken into 4 main parts:

**Object Controller** - instantiates and updates chaperone bound object, lighthouse objects, is the transform root of the camera which renders the UI view

**UI Controller** - handles all of the UI for the system (handles, buttons, text, etc.)

**Backend** - takes care of file reading and writing (serialization), fetching and storing calibration files.

**Tunnel Controller** - an interface to talk to the physical tunnel in a more standard way -- rather than just changing it's transform

Adhering to these logical distinctions should help any extension to this system continue to interact smoothly with the rest of the app.

### Asset Fading

The fading of world space text, world space images, and other things can be accomplished using `Assets\Generics\Fadeable.cs`.

### Player Detection

Detecting that the player has entered a certain are of the app and triggering certain events can be accomplished using `Assets\Generics\TriggerArea.cs`. To use, attach a `collider` to an object along with `TriggerArea`. Make sure `isTrigger` is checked on your collider. You can hook into the `OnTriggerAreaEntered` event to be notified when the player enters the area.

Note that this script depends on `Tags` to work. Currently, the `HeadCollider` tag is hardcoded in and accordingly, the `Player (Vauquois Variant)` prefab has a child object named and tagged `HeadCollider`. Any extension or change to the player prefab should include an object which follows the players head position and is tagged `HeadCollider`.

### Redirection

Redirection happens at specific points in the Vauquois tunnel experience. Each time the user gets to the end of the tunnel, they enter a redirection zone. Here, the rotation of their head is observed and used to rotate the scene around them in such a way that the rotation is unnoticeable, and walking down the next tunnel (which seems to be a 90 degree turn to them) results in them walking back exactly where they came from in physical space. Essentially, the user will walk back and fourth in straight lines in real life, while precieving walking through a series of tunnels each at a 90 degree turn from each other. The system that controls this was written a while ago. It contains commented and legacy code. It can be found in `Assets/Externals/FromRedirectedWalkingProject`.

### Settings

Vauquois stores information on the computer that is important for it to work properly. This includes calibration data (where the tunnel should be placed in relation to physical space), and general setting data including if this user is a first time user, if the opening scene should be skipped, and whether to use a controller or tracker for the lantern. All settings can be accessed and saved via `VauquoisSettings` as static properties and methods. `VauquoisSettings` uses `VauquoisFileIO` to do it's reading and writing.  `Application.persistentDataPath` is used for storage. 

## Misc. Information, Notes, & Future Work

The project needs to be reorganized and have old assets removed.

When developers are working on the new version with new tunnels and new scenes, they should use the new player detection system rather than the old deprecated one.

There are a number of other future plans which can be found in the ideas section of the [Asana](https://app.asana.com/0/526970137365668/board) used to manage this project. Contact aries-dev-g@vt.edu for access.

A generic system which handles fading on both Vive and Quest should be implemented. This system should either not be reliant on either platform, or perform platform detection, using the correct fading calls as needed.

The Diegetic Text Experiment still needs to be conducted. The latest version of this project can be found on the ``Text_Study`` branch of the main repository. 

# Attribution

## The Vauquois Experience

Todd Ogle (Project Lead), University Libraries, Virginia Tech
David Hicks (Learning Design), School of Education, Virginia Tech
Zach Duer (Creative Director), School of Visual Arts, Virginia Tech

## 3D Modeling, Texturing, Interface Design

Alex Forlini, School of Visual Arts, Virginia Tech
Brennan Young, School of Visual Arts, Virginia Tech
Huy Ngo, School of Visual Arts, Virginia Tech
Matthew Yourshaw, School of Visual Arts, Virginia Tech
Phat Nguyen, School of Visual Arts, Virginia Tech
Xindi Liu, School of Visual Arts, Virginia Tech
Zach Bush, School of Visual Arts, Virginia Tech
Bethany Lavery, School of Visual Arts, Virginia Tech
Nick Wyers, Engineering, Christopher Newport University

## Voice Acting

Patty Raun, School of Performing Arts, Virginia Tech
Herve Marand, Department of Chemistry, Virginia Tech

## Documentary Film

Gwendolyn Ogle, Galloping Muse Productions

## Programming

Zach Duer, School of Visual Arts, Virginia Tech
Dillon Cutaiar, Department of Computer Science, Virginia Tech
Nicolas Gutkowski, Department of Computer Science, Virginia Tech
Run Yu, Department of Computer Science, Virginia Tech
Doug Bowman, Department of Computer Science, Virginia Tech

## Field Work/Data Collection

Todd Ogle, University Libraries, Virginia Tech
David Hicks, School of Education, Virginia Tech
DongSoo Choi, School of Visual Arts, Virginia Tech
Thomas Tucker, School of Visual Arts, Virginia Tech
David Cline, Department of History, San Diego State University
Erik Westman, Department of Mining and Minerals Engineering, Virginia Tech
Daniel Newcomb, Engineering Education, Virginia Tech
Yves Massotte, Amis de Vauquois (Stewards of the Hill of Vauquois)
Melody Philippe, Amis de Vauquois (Stewards of the Hill of Vauquois)
Serge Thierion, Amis de Vauquois (Stewards of the Hill of Vauquois)
Francois LeGrand, Amis de Vauquois (Stewards of the Hill of Vauquois)
Adrien Arles, Arkemine
Celine Beauchamp (Site work coordinator), Arkemine
Phat Nguyen, School of Visual Arts, Virginia Tech
Sarah Tucker, University Libraries, Virginia Tech

## Primary Source Research

Celine Beauchamp, Arkemine (Rescue Archeology)
David Hicks, School of Education, Virginia Tech
David Cline, Department of History, San Diego State University
Daniel Newcomb, Engineering Education, Virginia Tech
Sarah Tucker, University Libraries, Virginia Tech

## Document Translation

Celine Beauchamp, Arkemine (Rescue Archeology)
Maureen Suess, School of Visual Arts, Virginia Tech
Scott Saverot, Biomedical Engineering and Sciences, Virginia Tech

## Exhibit Design and Construction

Scott Fralin, University Libraries, Virginia Tech
Caleb Carney, University Libraries, Virginia Tech
Molly Graham, University Libraries, Virginia Tech

## Funding Provided By:

The Virginia Tech Institute for Creativity, Arts, and Technology
Applied Research in Immersive Environments and Simulations, University Libraries at Virginia Tech
