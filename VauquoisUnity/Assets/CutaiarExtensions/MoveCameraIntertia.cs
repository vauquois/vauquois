﻿using UnityEngine;

// Modifications by Dillon Cutaiar on 7/12/18 in order to make the camera more useful and friendly.

// Credit to JISyed from https://gist.github.com/JISyed/7035473#file-movecamerainertia-cs
// for an almost working smooth camera script. Just needed a few fixes.

// Credit to damien_oconnell from http://forum.unity3d.com/threads/39513-Click-drag-camera-movement
// for using the mouse displacement for calculating the amount of camera movement and panning code.

namespace CutaiarExtensions
{
    public class MoveCameraIntertia : MonoBehaviour
    {
        //
        // VARIABLES
        //

        public float turnSpeed = 35.0f;         // Speed of camera turning when mouse moves in along an axis
        public float panSpeed = 360.0f;         // Speed of the camera when being panned
        public float zoomSpeed = 500.0f;        // Speed of the camera going back and forth
        public float moveSpeed = 1f;            // Speed of the camera mpving around


        public float turnDrag = 5.0f;           // RigidBody Drag when rotating camera
        public float panDrag = 3.5f;            // RigidBody Drag when panning camera
        public float zoomDrag = 3.3f;           // RigidBody Drag when zooming camera

        public bool mouseVisible = true;        // Can you see the cursor?

        private Vector3 mouseOrigin;            // Position of cursor when mouse dragging starts
        private bool isPanning;                 // Is the camera being panned?
        private bool isRotating;                // Is the camera being rotated?
        private bool isZooming;                 // Is the camera zooming?
        private bool isRising;                  // Is the camera rising?
        private bool isFalling;                 // Is the camera falling?
        private bool isLeft;                    // Is the camera moving left?
        private bool isRight;                   // Is the camera moving right?
        private bool VerticalRotateAllowed;     // Allow vertical rotation?

        //
        // AWAKE
        //

        void Awake()
        {
            // Setup camera physics properties
            gameObject.AddComponent<Rigidbody>();
            GetComponent<Rigidbody>().useGravity = false;
            Cursor.visible = mouseVisible;
        }

        //
        // UPDATE: For input
        //

        void Update()
        {
            // == Getting Input ==

            // Get the left mouse button
            if (Input.GetMouseButtonDown(0))
            {
                // Get mouse origin
                mouseOrigin = Input.mousePosition;
                isRotating = true;
            }

            // Get the right mouse button
            if (Input.GetMouseButtonDown(1))
            {
                // Get mouse origin
                mouseOrigin = Input.mousePosition;
                isPanning = true;
            }

            // Get the middle mouse button
            if (Input.GetMouseButtonDown(2))
            {
                // Get mouse origin
                mouseOrigin = Input.mousePosition;
                isZooming = true;
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                isRising = true;
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                isFalling = true;
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                isLeft = true;
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                isRight = true;
            }

            if (Input.GetKeyDown(KeyCode.V))
            {
                VerticalRotateAllowed = true;
            }


            // == Disable movements on Input Release ==

            if (!Input.GetMouseButton(0)) isRotating = false;
            if (!Input.GetMouseButton(1)) isPanning = false;
            if (!Input.GetMouseButton(2)) isZooming = false;
            if (!Input.GetKey(KeyCode.W)) isRising = false;
            if (!Input.GetKey(KeyCode.S)) isFalling = false;
            if (!Input.GetKey(KeyCode.A)) isLeft = false;
            if (!Input.GetKey(KeyCode.D)) isRight = false;
            if (!Input.GetKey(KeyCode.V)) VerticalRotateAllowed = false;


        }

        //
        // Fixed Update: For Physics
        //

        void FixedUpdate()
        {
            // == Movement Code ==

            // Rotate camera along X and Y axis
            if (isRotating)
            {
                // Get mouse displacement vector from original to current position
                Vector3 pos = GetComponent<Camera>().ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

                // Set Drag
                GetComponent<Rigidbody>().angularDrag = turnDrag;

                // Two rotations are required, one for x-mouse movement and one for y-mouse movement
                if (VerticalRotateAllowed)
                {
                    GetComponent<Rigidbody>().AddTorque(-pos.y * turnSpeed * transform.right, ForceMode.Acceleration);
                }
                else
                {
                    GetComponent<Rigidbody>().AddTorque(pos.x * turnSpeed * transform.up, ForceMode.Acceleration);
                }
            }

            // Move (pan) the camera on it's XY plane
            if (isPanning)
            {
                // Get mouse displacement vector from original to current position
                Vector3 pos = GetComponent<Camera>().ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
                Vector3 move = new Vector3(pos.x * panSpeed, pos.y * panSpeed, 0);

                // Apply the pan's move vector in the orientation of the camera's front
                Quaternion forwardRotation = Quaternion.LookRotation(transform.forward, transform.up);
                move = forwardRotation * move;

                // Set Drag
                GetComponent<Rigidbody>().drag = panDrag;

                // Pan
                GetComponent<Rigidbody>().AddForce(move, ForceMode.Acceleration);
            }

            // Move the camera linearly along Z axis
            if (isZooming)
            {
                // Get mouse displacement vector from original to current position
                Vector3 pos = GetComponent<Camera>().ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
                Vector3 move = pos.y * zoomSpeed * transform.forward;

                // Set Drag
                GetComponent<Rigidbody>().drag = zoomDrag;

                // Zoom
                GetComponent<Rigidbody>().AddForce(move, ForceMode.Acceleration);
            }

            // Move the camera linearly along the Vector3 Up axis
            if (isRising)
            {
                Vector3 pos = GetComponent<Camera>().transform.position;
                Vector3 move = pos.y * moveSpeed * Vector3.up;

                // Set Drag
                GetComponent<Rigidbody>().drag = panDrag;

                // Rise
                GetComponent<Rigidbody>().AddForce(move, ForceMode.Acceleration);
            }

            // Move the camera linearly along the Vector3 Down axis
            if (isFalling)
            {
                Vector3 pos = GetComponent<Camera>().transform.position;
                Vector3 move = pos.y * moveSpeed * Vector3.down;

                // Set Drag
                GetComponent<Rigidbody>().drag = panDrag;

                // Rise
                GetComponent<Rigidbody>().AddForce(move, ForceMode.Acceleration);
            }

            // Move the camera linearly to the left
            if (isLeft)
            {
                Vector3 pos = GetComponent<Camera>().transform.position;
                Vector3 move = pos.y * moveSpeed * -GetComponent<Camera>().transform.right;

                // Set Drag
                GetComponent<Rigidbody>().drag = panDrag;

                // Rise
                GetComponent<Rigidbody>().AddForce(move, ForceMode.Acceleration);
            }

            // Move the camera linearly to the right
            if (isRight)
            {
                Vector3 pos = GetComponent<Camera>().transform.position;
                Vector3 move = pos.y * moveSpeed * GetComponent<Camera>().transform.right;

                // Set Drag
                GetComponent<Rigidbody>().drag = panDrag;

                // Rise
                GetComponent<Rigidbody>().AddForce(move, ForceMode.Acceleration);
            }
        }

    }
}