﻿using UnityEngine;
using UnityEditor;
using System.Diagnostics;

namespace CutaiarExtensions
{
    public class BuildToOculusGoWindow : EditorWindow
    {

        string pathToApk;
        string nameOfApk;
        string nameOfBundle;

        string output;

        private void OnEnable()
        {
            pathToApk = EditorPrefs.GetString("pathToApk");
            nameOfApk = EditorPrefs.GetString("nameOfApk");
            nameOfBundle = EditorPrefs.GetString("nameOfBundle");
        }

        private void OnDisable()
        {
            EditorPrefs.SetString("pathToApk", pathToApk);
            EditorPrefs.SetString("nameOfApk", nameOfApk);
            EditorPrefs.SetString("nameOfBundle", nameOfBundle);
        }

        [MenuItem("Window/Oculus Go Build")]
        public static void ShowWindow()
        {
            GetWindow<BuildToOculusGoWindow>("Oculus Go Build");
        }

        private void OnGUI()
        {
            GUILayout.Label("Conveniently Build to Oculus Go", EditorStyles.boldLabel);

            pathToApk = EditorGUILayout.TextField("Path to APK", pathToApk);
            nameOfBundle = EditorGUILayout.TextField("Name of Bundle", nameOfBundle);
            nameOfApk = EditorGUILayout.TextField("Name of APK", nameOfApk);

            EditorGUILayout.TextArea(output, GUILayout.Height(position.height / 3));

            if (GUILayout.Button("Build"))
            {
                UnityEngine.Debug.Log("Building to Oculus Go...");
                output = "Building to Oculus Go...\n";

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WorkingDirectory = "/WINDOWS/system32";
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/c cd" + pathToApk + " && adb uninstall " + nameOfBundle + " && adb install " + nameOfApk;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;
                Process proc = Process.Start(startInfo);


                output += proc.StandardOutput.ReadToEnd();
                output += proc.StandardError.ReadToEnd();
                output += "Build Done.";
                UnityEngine.Debug.Log("Build Done.");
            }
        }
    }
}
