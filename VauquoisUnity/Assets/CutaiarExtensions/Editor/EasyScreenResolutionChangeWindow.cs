﻿using UnityEditor;
using System.Reflection;
using UnityEngine;

// This editor script allows the developer to easily change the game window resolution.
// It makes it easy to see if the app scales to all resolutions nicely.
// Code taken from: https://answers.unity.com/questions/956123/add-and-select-game-view-resolution.html?_ga=2.72134230.490327432.1580165240-408662477.1522077723

namespace CutaiarExtensions
{
    public class EasyScreenResolutionChangeWindow : EditorWindow
    {
        int index;
        int defaultIndex;

        [MenuItem("Window/Easy Screen Resolution")]
        public static void ShowWindow()
        {
            GetWindow<EasyScreenResolutionChangeWindow>("Easy Screen Resolution");
        }

        void OnGUI()
        {
            EditorGUI.BeginChangeCheck();
            index = EditorGUILayout.IntSlider(index, 0, GetNumberOfResolutions());
            if (EditorGUI.EndChangeCheck())
            {
                SetIndex(index);
                // TODO: Display name of current resolution
            }

            defaultIndex = EditorGUILayout.IntField("Default Index", defaultIndex);
            if (GUILayout.Button("Reset to Default"))
            {
                index = defaultIndex;
                SetIndex(defaultIndex);
            }
        }

        private static void SetIndex(int index)
        {
            var gvWndType = typeof(Editor).Assembly.GetType("UnityEditor.GameView");
            var gvWnd = EditorWindow.GetWindow(gvWndType);
            var SizeSelectionCallback = gvWndType.GetMethod("SizeSelectionCallback", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            SizeSelectionCallback.Invoke(gvWnd, new object[] { index, null });
        }

        private static int GetNumberOfResolutions()
        {
            var sizesType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizes");
            var singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
            var instanceProp = singleType.GetProperty("instance");
            var getGroup = sizesType.GetMethod("GetGroup");
            var gameViewSizesInstance = instanceProp.GetValue(null, null);

            var group = getGroup.Invoke(gameViewSizesInstance, new object[] { (int)GameViewSizeGroupType.iOS });
            var groupType = group.GetType();
            var getBuiltinCount = groupType.GetMethod("GetBuiltinCount");
            var getCustomCount = groupType.GetMethod("GetCustomCount");
            int sizesCount = (int)getBuiltinCount.Invoke(group, null) + (int)getCustomCount.Invoke(group, null);
            return sizesCount - 1;
        }
    }
}
