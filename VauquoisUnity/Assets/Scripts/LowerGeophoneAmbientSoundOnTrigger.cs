﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowerGeophoneAmbientSoundOnTrigger : MonoBehaviour {

    public AudioSource ambientSoundSource;

    private float origVol;

    bool bFadingIn = false;
    bool bFadingOut = false;

	// Use this for initialization
	void Start () {
        origVol = ambientSoundSource.volume;
	}
	
	// Update is called once per frame
	void Update () {
        if (bFadingIn) {
            if (ambientSoundSource.volume < origVol) {
                ambientSoundSource.volume += 0.003f;
            }
        }
        if (bFadingOut) {
            if (ambientSoundSource.volume > origVol / 3) {
                ambientSoundSource.volume -= 0.001f;
            }
        }
	}

    void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
            Debug.Log("on trigger enter");
            bFadingOut = true;
            bFadingIn = false;
        }
    }

    void OnTriggerExit(Collider collider) {
        if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
            Debug.Log("on trigger exit");
            bFadingIn = true;
            bFadingOut = false;
        }
    }


}
