﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerVO_Special_SmithsonianHallwayOne : MonoBehaviour {

    public VoiceOverManager voiceOverManager;
    public AudioClip faceCarvingAudioClip;

    public Light faceCarvingSpotLight;

    bool bTriggered = false;

    private void Start()
    {
        faceCarvingSpotLight.intensity = 0;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (Time.time > 0.1f)
        {
            if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider")
            {
                if (!bTriggered)
                {
                    voiceOverManager.PlayOrQueueAudio(faceCarvingAudioClip);
                    bTriggered = true;
                    //StartCoroutine(FaceCarvingLightAndVO());
                }
            }
        }
    }

    IEnumerator FaceCarvingLightAndVO()
    {
         StartCoroutine(FadeLightIn(faceCarvingSpotLight, 1.3f, 2f));
        yield return new WaitForSeconds(25);
        StartCoroutine(FadeLightOut(faceCarvingSpotLight, 0f, 2f));
    }

    private IEnumerator FadeLightIn(Light light, float targetIntensity, float time)
    {
        while (light.intensity < targetIntensity)
        {
            light.intensity += .015f;
            yield return new WaitForSeconds(time / 60f); // divide the time to fade by a guessed at frame rate
        }
    }

    private IEnumerator FadeLightOut(Light light, float targetIntensity, float time)
    {
        while (light.intensity > targetIntensity)
        {
            light.intensity -= .015f;
            yield return new WaitForSeconds(time / 60f); // divide the time to fade by a guessed at frame rate
        }
    }
}
