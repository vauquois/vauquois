using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TriggerUI : MonoBehaviour {

	public GameObject UIElement;

	private bool bTriggered = false;

	void Start(){
		UIElement.GetComponent<TextMeshProUGUI> ().color = new Color (1, 1, 1, 0);
	}

	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
			if (!bTriggered) {
				Debug.Log ("start fading in UI end message");
				StartCoroutine("FadeInUI");
				bTriggered = true;
			}
		}
	}

	public void TriggerFadeInUI(){
		if (!bTriggered) {
			Debug.Log ("start fading in UI end message");
			StartCoroutine("FadeInUI");
			bTriggered = true;
		}
	}

	IEnumerator FadeInUI() {
		for (float f = 0f; f <= 1; f += 0.001f) {
			//Debug.Log ("f: " + f);
			Color c = UIElement.GetComponent<TextMeshProUGUI> ().color;
			//c.r = f;
			//c.g = f;
			//c.b = f;
			c.a = f;
			UIElement.GetComponent<TextMeshProUGUI> ().color = c;
			yield return new WaitForSeconds(.01f);
		}
	}
}
