﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerParticleSystem : MonoBehaviour {

    public ParticleSystem[] particleSystems;

    bool bHasTriggered = false;

	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerEnter(Collider collider) {
		if (Time.time > 2) {
			if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
				if (!bHasTriggered) {
					for (int i = 0; i < particleSystems.Length; i++) {
						particleSystems [i].Play ();
					}
					bHasTriggered = true;
				}
			}
		}
    }
}
