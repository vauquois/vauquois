﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Valve.VR.InteractionSystem
{

    public class TrackedObjectIndexSetter : MonoBehaviour
    {
        void Start()
        {
            SteamVR_TrackedObject tr = GetComponent<SteamVR_TrackedObject>();

            uint index = 0;
            var error = ETrackedPropertyError.TrackedProp_Success;
            for (uint i = 0; i < 16; i++)
            {
                var result = new System.Text.StringBuilder((int)64);
                OpenVR.System.GetStringTrackedDeviceProperty(i, ETrackedDeviceProperty.Prop_RenderModelName_String, result, 64, ref error);
                Debug.Log("tracked device name: " + result);
                if (result.ToString().Contains("vr_tracker"))
                {
                    index = i;
                    Debug.Log("Tracked Object Device Index: " + index);
                    Debug.Log("Setting to proper index...");
                    tr.index = (SteamVR_TrackedObject.EIndex) index;
                    gameObject.SetActive(true);
                    break;
                }
            }
        }
    }
}
