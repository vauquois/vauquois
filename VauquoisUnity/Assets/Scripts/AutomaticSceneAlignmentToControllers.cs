﻿using System;
using UnityEngine;
using UnityEngine.UI;

[ObsoleteAttribute("This aligment class is obsolete. Use CalibrationTunnelController instead.")]
public class AutomaticSceneAlignmentToControllers : MonoBehaviour {

    [Tooltip("Check this if you want to correct the height of the tunnel so that every person " +
             "starts at the same head height regardless of their real world height. Note that " +
             " height adjustment happens at the start of the experience and uses the current " +
             "height of the HMD. THE PARTICIPANT MUST BE STNADING WITH THE HMD ON AT THE START " +
             "OF THE EXPERIENCE FOR THIS TO PRODUCE EXPECTED RESULTS.")]
    public bool shouldFixHeight = false;

    // Locations which guide the allighment of the "scene" GO
    public Transform scnAnchor1_start;
    public Transform scnAnchor2_corner;

    public Transform playerStartVirtual;
    public Transform playerCamera;

    // The locations of the hands
    public Transform trackedAnchor01_start;
    public Transform trackedAnchor02_corner;

    // The date of the last calibration
    string lastCalib;

    // A Text object to tell the user what's going on
    public Text text;

    // Load a calibration if present
    private void Start()
    {
        // No calibration present (probobly)
        if (PlayerPrefs.GetFloat("xPos") == 0.0f) //check date instead?
        {
            Debug.LogWarning("No saved calibration data available. Scene will be aligned at (0, 0, 0).");
        }

        // There is already a calibration present, load that
        loadFromPrefs();
        updateText();
        if (shouldFixHeight) fixHeight();
    }

    // align the scene and write the position to prefs
    public void align()
    {
        //Debug.Log("Calibration alignment happening");
        // make sure before doing this that hand1 is at "far" end of the tunnel, corresponding with the virtual position of scene anchor1
        Vector3 diff1 = trackedAnchor01_start.position - scnAnchor1_start.position;
        //Debug.Log("trackedAnchor01_start.position: " + trackedAnchor01_start.position.ToString("F3"));
        //Debug.Log("scnAnchor1_start.position: " + scnAnchor1_start.position.ToString("F3"));

        //Debug.Log("newPos: " + new Vector3(transform.position.x + diff1.x, transform.position.y + diff1.y, transform.position.z + diff1.z).ToString("F3"));
        transform.position = new Vector3(transform.position.x + diff1.x, transform.position.y, transform.position.z + diff1.z);

        Vector3 diff2 = scnAnchor1_start.position - trackedAnchor02_corner.position;
        Vector3 diff3 = scnAnchor1_start.position - scnAnchor2_corner.position;
        transform.RotateAround(scnAnchor1_start.position, Vector3.up, Vector3.SignedAngle(diff3, diff2, Vector3.up));
       // Debug.Log("rotation: " + Vector3.SignedAngle(diff3, diff2, Vector3.up).ToString("F3"));

        writeToPrefs();
        updateText();

    }

    private void fixHeight()
    {
        float yCorrection = playerStartVirtual.transform.position.y - playerCamera.transform.position.y;
        transform.position = new Vector3(transform.position.x, transform.position.y - yCorrection, transform.position.z);
        Debug.Log($"HeightFix enabled. Tunnel was adjusted {yCorrection} meters.");
    }

    // Show information
    public void updateText()
    {
        string info = string.Empty;
        string date = PlayerPrefs.GetString("date");

        if (date == "")
        {
            info = "No calibration data saved.";
            
        } else
        {
            info = "The last calibration was: " + date;
        }

        text.text = info;
    }

    // Write the current state of the scene to the player prefs
    public void writeToPrefs()
    {
        // Grab time
        string date = System.DateTime.Now.ToLongDateString() + " at " + System.DateTime.Now.ToLongTimeString();
        PlayerPrefs.SetString("date", date);

        PlayerPrefs.SetFloat("xPos", transform.position.x);
        PlayerPrefs.SetFloat("yPos", transform.position.y);
        PlayerPrefs.SetFloat("zPos", transform.position.z);

        PlayerPrefs.SetFloat("xRot", transform.rotation.x);
        PlayerPrefs.SetFloat("yRot", transform.rotation.y);
        PlayerPrefs.SetFloat("zRot", transform.rotation.z);
        PlayerPrefs.SetFloat("wRot", transform.rotation.w);

        PlayerPrefs.Save();
        Debug.Log("Saved Calibration");
    }

    // Load position based on prefs
    public void loadFromPrefs()
    {
        Debug.Log("Calibration data found. Scene will be aligned.");
        transform.position = new Vector3( PlayerPrefs.GetFloat("xPos"), transform.position.y, PlayerPrefs.GetFloat("zPos"));
        transform.rotation = new Quaternion(PlayerPrefs.GetFloat("xRot"), PlayerPrefs.GetFloat("yRot"), PlayerPrefs.GetFloat("zRot"), PlayerPrefs.GetFloat("wRot"));
    }
}
