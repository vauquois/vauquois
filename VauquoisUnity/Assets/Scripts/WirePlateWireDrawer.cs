﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WirePlateWireDrawer : MonoBehaviour {

    public GameObject[] wirePlateCylindersTop;
    public GameObject[] wirePlateCylindersBottom;
    public Material mat;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnPostRender() {
        Debug.Log("on post render");
        if (!mat) {
            Debug.LogError("Please Assign a material on the inspector");
            return;
        }
        GL.PushMatrix();
        mat.SetPass(0);
        GL.LoadProjectionMatrix(Camera.main.projectionMatrix);
        GL.MultMatrix(Camera.main.worldToCameraMatrix);
        GL.Begin(GL.LINES);
        for (int i = 0; i < wirePlateCylindersTop.Length - 1; i++) {
            Debug.Log("from " + wirePlateCylindersTop[i].transform.position + " to " + wirePlateCylindersTop[i + 1].transform.position);
            GL.Color(Color.red);
            GL.Vertex(wirePlateCylindersTop[i].transform.position);
            GL.Vertex(wirePlateCylindersTop[i + 1].transform.position);
        }

        GL.End();

        GL.Begin(GL.LINES);
        for (int i = 0; i < wirePlateCylindersBottom.Length - 1; i++) {
            GL.Color(Color.red);
            GL.Vertex(wirePlateCylindersBottom[i].transform.position);
            GL.Vertex(wirePlateCylindersBottom[i + 1].transform.position);
        }
        GL.End();
        GL.PopMatrix();
    }
}
