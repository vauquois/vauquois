﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerAudio : MonoBehaviour {

    public AudioClip[] audioClips;

    AudioSource audioSource;

    float movementDeltaAvg;
    List<Vector3> recordedPositions = new List<Vector3>();

    bool bFadingIn = false;
    bool bFadingOut = false;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        // calculate movement delta
        recordedPositions.Add(transform.localPosition);

        float deltaTotal = 0;
        for (int i = 1; i < recordedPositions.Count; i++) {
            deltaTotal += Vector3.Distance(recordedPositions[i], recordedPositions[i - 1]);
        }
        //Debug.Log("deltaTotal: " + deltaTotal);
        float movementDeltaAvg = deltaTotal / recordedPositions.Count; // average movement delta
        //Debug.Log("movementDeltaAvg: " + movementDeltaAvg);

        if (recordedPositions.Count >= 5) {
            recordedPositions.RemoveAt(0);
        }

		// if the audio source is not playing and movement delta is above X, play audio
        if (movementDeltaAvg > 0.01f) {
            if (!audioSource.isPlaying) {
                int rand = Random.Range(0, audioClips.Length);
                audioSource.clip = audioClips[rand];
                audioSource.Play();
                audioSource.time = Random.Range(0.0f, 5.0f);
                bFadingIn = true;
                bFadingOut = false;
            }
        }


        // if the audio source is playing and movement delta is below x, stop audio
        if (movementDeltaAvg < 0.0025f) {
            if (audioSource.isPlaying) {
                bFadingIn = false;
                bFadingOut = true;
                
            }
        }

        if (bFadingIn) {
            if(audioSource.volume < .5f) {
                audioSource.volume += 0.1f;
            }
        }
        if (bFadingOut) {
            if (audioSource.volume > 0) {
                audioSource.volume -= 0.1f;
            }
            else {
                audioSource.Stop();
            }
        }
	}
}
