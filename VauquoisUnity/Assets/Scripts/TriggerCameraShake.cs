﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCameraShake : MonoBehaviour {

    public GameObject camera;
    public bool useCustomShakeParameters = false;

    public float cameraShakeIntensity = 0.01f;
    public float cameraShakeDecay = 0.0001f;

    bool bCameraShook = false;

    void OnTriggerEnter(Collider collider) {
        if (Time.time > 2) {
            if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
                if (!bCameraShook) {
                    if (useCustomShakeParameters)
                    {
                        camera.GetComponent<ShakeCamera>().shakeIntensity = cameraShakeIntensity;
                        camera.GetComponent<ShakeCamera>().shakeDecay = cameraShakeDecay;
                    }
                    camera.GetComponent<ShakeCamera>().DoShake();
                    bCameraShook = true;
                }
            }
        }
    }
}
