﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugOutPosition : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log(name + "World Position: " + transform.position);
        }
	}

    // (1.8, 0.0, 1.7)
    // (-1.8, 0.0, -1.2)
}
