﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSound : MonoBehaviour {

    bool bAudioPlayed = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider collider) {
		if (Time.time > 2) {
			if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
				if (!bAudioPlayed) {
					GetComponent<AudioSource> ().Play ();
					bAudioPlayed = true;
				}
			}
		}
    }
}
