﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageController_RedirectionZone : MonoBehaviour {


	public GameObject[] images = new GameObject[3];
	public float imageFadeInTime = 2;
	public float imageFadeOutTime = 2;

	bool bSequenceTriggered= false;

	void Start(){
		for (int i = 0; i < images.Length; i++) {
			images[i].GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		}
	}

	void OnTriggerEnter(Collider collider) {
		//Debug.Log ("entered images collider");
		if (!bSequenceTriggered) {
			bSequenceTriggered = !bSequenceTriggered;
			for (int i = 0; i < images.Length; i++) {
				StartCoroutine(FadeInMaterial (images[i].GetComponent<Renderer> ().material, imageFadeInTime));
			}
		}
	}

	public void FadeOutImages(){
		for (int i = 0; i < images.Length; i++) {
			StartCoroutine(FadeOutMaterial (images[i].GetComponent<Renderer> ().material, imageFadeInTime));
		}
	}

	private IEnumerator FadeInMaterial(Material mat, float time){
		while (mat.color.a < 1){
			mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a + .015f);
			yield return new WaitForSeconds (time / 60f); // divide the time to fade by a guessed at frame rate
		}
	}

	private IEnumerator FadeOutMaterial(Material mat, float time){
		while (mat.color.a > 0){
			mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a - .015f);
			yield return new WaitForSeconds (time / 60);
		}
	}
}
