﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CalibrationBackend;

public class CalibrationTunnelController : MonoBehaviour
{
    // Note about how this component should be at the beginning of the tunnel

    [Header("Anchors")]

    [SerializeField]
    private Transform endAnchor;

    private Transform ManipulableTransform
    {
        get => transform;
        // Note that mutating ManipulableTransform.rotation etc may not have desired effects
    }

    public Vector3 TunnelStart
    {
        get { return ManipulableTransform.position; }
        set
        {
            ManipulableTransform.position = value;
        }
    }

    private void Update()
    {
        Debug.DrawLine(TunnelStart, TunnelEnd);
    }
    public Vector3 TunnelEnd
    {
        get { return endAnchor.position; }
    }

    public Vector3 Direction
    {
        get { return TunnelEnd - TunnelStart; }
        set
        {
            transform.rotation = Quaternion.LookRotation(value, Vector3.up);
        }
    }

    public float Length
    {
        get { return Vector3.Distance(TunnelEnd, TunnelStart); }
    }

    public float Width
    {
        get { return 1; }
    }

    // Events
    public static Action OnTunnelAligned;


    private void OnEnable()
    {
        CalibrationUIController.OnSaveClicked += SaveCurrentTransform;

        CalibrationUIController.OnAutoAlignButtonClicked += AlignToSteamVRChaperoneDiagonal;
    }

    // Dont eworry this is called on OnDestory as well
    private void OnDisable()
    {
        CalibrationUIController.OnSaveClicked -= SaveCurrentTransform;

        CalibrationUIController.OnAutoAlignButtonClicked -= AlignToSteamVRChaperoneDiagonal;
    }

    // Load a calibration if present
    private void Start()
    {
        if (CalibrationBackend.CalibrationPresent())
        {
            Debug.Log("Calibration data found. Scene will be aligned.");

            // There is already a calibration present, load that
            AlignToCurrentSave();
        }
        else
        {
            // Do a default aligment
            Debug.LogWarning("No saved calibration data available. Scene will be aligned SteamVR Chaperone Diagonal A");
            AlignToSteamVRChaperoneDiagonal('a');
        }
    }

    private void AlignToSteamVRChaperoneDiagonal(char c)
    {
        // Get objectController
        CalibrationObjectController objController = FindObjectOfType<CalibrationObjectController>();
        if (objController == null)
        {
            Debug.LogWarning($"{GetType().Name} could not find {typeof(CalibrationObjectController).Name} to use for defualt calibration!");
            return;
        }
        // Get diagonal
        Tuple<Vector3, Vector3> axis = c == 'a' ? objController.DiagonalA : objController.DiagonalB;

        // Align to it
        TunnelStart = axis.Item1;
        Direction = axis.Item2 - axis.Item1;

        OnTunnelAligned.Invoke();
    }

    private void AlignTo(CalibrationData data)
    {
        Vector3 tempPos = data.Position;
        if (!data.yValid) tempPos.y = ManipulableTransform.position.y;
        ManipulableTransform.position = tempPos;
        ManipulableTransform.rotation = data.Rotation;

        OnTunnelAligned?.Invoke();
    }

    public void SaveCurrentTransform()
    {
        CalibrationBackend.Save(ManipulableTransform);
    }

    public void AlignToCurrentSave()
    {
        CalibrationData data = CalibrationBackend.LoadMostRecent();
        AlignTo(data);
    }
}
