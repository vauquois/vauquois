# Calibration System Documentation

**Contact**: Dillon Cutaiar (dillonc@vt.edu)

## To Use
Add a "CalibrationMenu" prefab into your scene. This must be a child of a canvas, either already existing or newly created to hold this prefab.
Add a "CalibrationObjectController" to your scene.
Be sure that your tunnel is arranged in compliance with the "Tunnel Structure" section below.

Once you enter the scene, if a calibration file is found, the tunnel will be aligned to it. If one is not found, the tunnel's transform will remain unchanged. 
The handles on the calibration UI can then be used to position the tunnel before pressing the "Save Alignment" button.
The scene should then be restarted to ensure that everything works correctly from the start. Do not calibrate and then have someone use the program without restarting. Other systems may not work if this is done.

## Tunnel Structure
To be compliant with this system, the tunnel being used should adhere to a specific heirarchy structure. An example of this structure can be found in the "Tunnel Example" prefab.

The root of the tunnel should be an empty with one component on it: CalibrationTunnelController. 
There should be two children of this empty:
* an empty object which indicates the end of the tunnel (I called mine tunnelEnd)
* Your tunnel, such that the z-axis (blue) of its parent points down the tunnel and the x-axis (red) of its parent points to the right. It is okay to rotate your tunnel in whatever way you need to to make these statements true.

Note that you should now move the tunnelEnd object to the end of your tunnel (If everything is correct so far, you should be moving this object mainly on the z axis).
Set CalibrationTunnelController.EndAnchor = your tunnel end object
You should put a TunnelGraphicPrefab as a child of your tunnel and make sure that the graphic roughly lines up with the size and shape of your tunnel. This will allow the tunnel to be displayed in the UI.
Your tunnel is now compliant with this system.

It might be a good idea to have this root tunnel object at (0, 0, 0) in your scene.

## System Detials
The system is split into 4 main parts:
* Object Controller - instantiates and updates chaperone bound object, lighthouse objects, is the transform root of the camera which renders the UI view
* UI Controller     - handles all of the UI for the system (handles, buttons, text, etc.)
* Backend           - takes care of file reading and writing (serialization), fetching and storing calibration files.
* Tunnel Controller - an interface to talk to the physical tunnel in a more standard way -- rather than just changing it's transform

Please note there is some breakdown of encapsulation -- the system isnt perfect. I.e. UI Controller moves physical tunnel, Tunnel Controller has a "Save" function which talks to the backend, etc.

Data is saved here: in this format: