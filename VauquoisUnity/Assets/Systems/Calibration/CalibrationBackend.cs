﻿using System;
using UnityEngine;

public static class CalibrationBackend
{
    // The data most recntly retrieved or null if nothing has been retrieved yet
    public static CalibrationData CurrentData { get; private set; }

    public static Action OnSave;

    private static string settingsFileName = "VauquoisCalibrationData.json";

    // Is there a calibration stored on disk?
    public static bool CalibrationPresent()
    {
        // No calibration present (probobly)
        return VauquoisFileIO.FileExists(settingsFileName);
    }

    // Write the current state of the scene to the player prefs
    public static void Save(Transform t)
    {
        // Grab time
        string date = System.DateTime.Now.ToShortDateString() + " at " + System.DateTime.Now.ToShortTimeString();

        // Save the current transfrom as json
        CalibrationData data = new CalibrationData(t.position, t.rotation, date, false);
        string dataJSON = JsonUtility.ToJson(data);
        VauquoisFileIO.WriteToFile(settingsFileName, dataJSON);
        Debug.Log($"CalibrationBackend: Saved Calibration\ndata: {dataJSON}\nFile: {settingsFileName}");

        // Keep our current data up to date
        CurrentData = data;
        
        // Update settings so that systems reliant on this flag work
        VauquoisSettings.hasCompletedFirstCalibration = true;
        VauquoisSettings.Save();

        // Let everyone know we saved
        OnSave?.Invoke();
    }

    // Get the most recent data from disk
    public static CalibrationData LoadMostRecent()
    {
        string dataJSON = "";
        try
        {
            dataJSON = VauquoisFileIO.ReadFromFile(settingsFileName);
        }
        catch (Exception e) //TODO: more error checking
        {
            Debug.LogError("There was an issue loading the calibration from disk");
            return new CalibrationData(Vector3.zero, Quaternion.identity, "", false);
        }
        CalibrationData data = JsonUtility.FromJson<CalibrationData>(dataJSON);
        if (data == null)
        {
            Debug.LogError("There was an issue loading the calibration from disk");
            return new CalibrationData(Vector3.zero, Quaternion.identity, "", false);
        }

        CurrentData = data;
        return CurrentData;
    }

    [Serializable]
    public class CalibrationData
    {
        public Vector3 Position; // { get; private set; } Cant use with JSONUtility :(
        public Quaternion Rotation;
        public string Date;
        public bool yValid;

        public CalibrationData(Vector3 _position, Quaternion _rotation, string _date, bool _yValid)
        {
            Position = _position;
            Rotation = _rotation;
            Date = _date;
            yValid = _yValid;
        }

        public override string ToString()
        {
            return $"CalibrationData(Position: {Position}, Rotation: {Rotation}, Date: {Date}, yValid: {yValid})";
        }
    }
}
