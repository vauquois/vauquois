﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Valve.VR;
using System;

/*
 * 
 * This script is heavily based on the structure of SteamVR_TrackingReferenceManager found in Assets/SteamVR/Scripts
 * 
 * More information about OpenVR and SteamVR was gathered here:
 * https://gamedev.stackexchange.com/questions/119943/what-is-the-proper-way-to-get-input-from-the-motion-controllers-touch-pads
 * https://github.com/ValveSoftware/openvr/wiki/IVRSystem::GetTrackedDeviceClass
 * https://stackoverflow.com/questions/45860112/how-to-detect-position-of-vive-base-stations
 * https://stackoverflow.com/questions/43184610/how-to-determine-whether-a-steamvr-trackedobject-is-a-vive-controller-or-a-vive
 * https://www.reddit.com/r/Unity3D/comments/81sfbm/getting_play_area_size_for_vive_question/
 * https://github.com/ValveSoftware/openvr/wiki/API-Documentation
 * 
 */
public class CalibrationObjectController : MonoBehaviour
{

    [Header("Assignments")]

    [SerializeField] private GameObject TrackedReference_Scene_Space_Indicator_Prefab = null;
    [SerializeField] private GameObject TrackedSpaceCorner_Scene_Space_Indicator_Prefab = null;

    [SerializeField] private LineRenderer lineRenderer;

    [SerializeField] private GameObject calibrationCamera;

    [Header("Settings")]

    [SerializeField] private bool shouldScaleRenderModels = false;
    [SerializeField, Layer] private int CalibrationObjectLayer = 0;
    [SerializeField] private float calibrationCameraScaleConstant = 0.8f;

    public Tuple<Vector3, Vector3> DiagonalA;
    public Tuple<Vector3, Vector3> DiagonalB;

    // Start is called before the first frame update
    void Awake()
    {
        // Set up chaperone objects
        FindChaperoneBounds();

        // Draw linerenderer

        // Set up tunnel object

        // Lighthouses are tken care of

        // Remember to transform and scale camera to fit bounds

    }

    private void OnEnable()
    {
        SteamVR_Events.NewPoses.AddListener(OnNewPoses);
    }

    private void OnDisable()
    {
        SteamVR_Events.NewPoses.RemoveListener(OnNewPoses);
    }


    // Update is called once per frame
    void Update()
    {
        Debug.DrawLine(DiagonalA.Item1, DiagonalA.Item2, Color.red);
        Debug.DrawLine(DiagonalB.Item1, DiagonalB.Item2, Color.blue);
    }

    private void updateLineRenderer(Vector3[] vertices)
    {
        // TODO: check for danger accessing array like this
        lineRenderer.positionCount = vertices.Length+1;
        lineRenderer.SetPositions(vertices);
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, vertices[0]);
    }

    void FindChaperoneBounds()
    {
        var pRect = new HmdQuad_t();
        var chaperone = OpenVR.Chaperone;
        bool checkPlayArea = (chaperone != null) && chaperone.GetPlayAreaRect(ref pRect);

        Debug.Log($"Play Area Bounds:\n" +
                  $"Corner 0 (xyz): ({pRect.vCorners0.v0},{pRect.vCorners0.v1},{pRect.vCorners0.v2})\n" +
                  $"Corner 1 (xyz): ({pRect.vCorners1.v0},{pRect.vCorners1.v1},{pRect.vCorners1.v2})\n" +
                  $"Corner 2 (xyz): ({pRect.vCorners2.v0},{pRect.vCorners2.v1},{pRect.vCorners2.v2})\n" +
                  $"Corner 3 (xyz): ({pRect.vCorners3.v0},{pRect.vCorners3.v1},{pRect.vCorners3.v2})");


        var corners = new HmdVector3_t[] { pRect.vCorners0, pRect.vCorners1, pRect.vCorners2, pRect.vCorners3 };
        Vector3[] vertices = new Vector3[corners.Length];
        for (int i = 0; i < corners.Length; i++)
        {
            var c = corners[i];
            vertices[i] = new Vector3(c.v0, c.v1, c.v2);
        }
        updateLineRenderer(vertices);

        // Calculate diagonals (only if we have a rectangle)
        if (vertices.Length == 4)
        {
            DiagonalA = new Tuple<Vector3, Vector3>(vertices[0], vertices[2]);
            DiagonalB = new Tuple<Vector3, Vector3>(vertices[1], vertices[3]);
        }

        // Initialize calibration camera
        InitCalibrationCamera();

        // If you want objects in each corner
        //foreach (Vector3 v in vertices)
        //{
        //    Instantiate(TrackedSpaceCorner_Scene_Space_Indicator_Prefab, v, Quaternion.identity, transform);
        //}

        // STEAMVR EXAMPLE CODE
        /*
        var rect = new HmdQuad_t();
        SteamVR_PlayArea.Size size;
        if (!SteamVR_PlayArea.GetBounds(size, ref rect))
            return;

        var corners = new HmdVector3_t[] { rect.vCorners0, rect.vCorners1, rect.vCorners2, rect.vCorners3 };

        Vector3[] vertices = new Vector3[corners.Length * 2];
        for (int i = 0; i < corners.Length; i++)
        {
            var c = corners[i];
            vertices[i] = new Vector3(c.v0, 0.01f, c.v2);
        }
        */
    }

    void InitCalibrationCamera()
    {
        // Dont need to set position of camera as steamVR origin is always 0,0,0

        // Set size of camera to accomodate any bounds
        // TODO: Base this off of lighthouses rather than play bounds
        calibrationCamera.GetComponent<Camera>().orthographicSize = calibrationCameraScaleConstant * Vector3.Distance(DiagonalA.Item1, DiagonalA.Item2);
    }

    // Deprecated
    void FindLighthouses()
    {
        // Do it in one go
        Debug.Log("GetTrackedReferenceIndicies_Experimental:");
        GetTrackedReferenceIndicies_Experimental().ForEach(i => Debug.Log(i));

        // Do it the "right" way
        Debug.Log("GetTrackedReferenceIndicies:");
        GetTrackedReferenceIndicies().ForEach(i => Debug.Log($"i: {i}"));

        // Instantiate reference objects for viewing
        
    }

    // Deprecated
    // Get the device indicies for the tracked reference devices that are currently active
    // Note that Tracked Reference = lighthouse = base station. Just choosing to use OpenVR terminology.
    private List<uint> GetTrackedReferenceIndicies()
    {
        List<uint> indicies = new List<uint>();
        for (uint i = 0; i < OpenVR.k_unMaxTrackedDeviceCount; i++)
        {
            ETrackedDeviceClass deviceClass = OpenVR.System.GetTrackedDeviceClass(i);
            if (deviceClass == ETrackedDeviceClass.TrackingReference)
            {
                indicies.Add(i);
            }
        }
        return indicies;
    }

    // Deprecated
    // Get the indicies of the lighthouses using GetSortedTrackedDeviceIndicesOfClass
    // WARNING: There are issues with this method. Use GetTrackedReferenceIndicies() instead.
    private List<uint> GetTrackedReferenceIndicies_Experimental()
    {
        List<uint> lightHouseIndexesForReturn = new List<uint>();
        uint[] baseStationIndexes = new uint[OpenVR.k_unMaxTrackedDeviceCount];
        OpenVR.System.GetSortedTrackedDeviceIndicesOfClass(ETrackedDeviceClass.TrackingReference, 
                                                           baseStationIndexes, 
                                                           OpenVR.k_unTrackedDeviceIndex_Hmd); /*unchecked((uint) -1) Maybe this should be -1 for global space instead?*/
        foreach (uint t in baseStationIndexes)
        {
            // TODO: This line might be an error, as maybe a device could be indexed zero, 
            // but otherwise how do you know if this is just extra space in the array?
            if (t != 0) 
            {
                lightHouseIndexesForReturn.Add(t);
            }
        }
        return lightHouseIndexesForReturn;
    }

    private Dictionary<uint, TrackingReferenceObject> trackingReferences = new Dictionary<uint, TrackingReferenceObject>();


    // Derived from steamVR's TrackingReferenceManager
    private void OnNewPoses(TrackedDevicePose_t[] poses)
    {
        if (poses == null)
            return;

        for (uint deviceIndex = 0; deviceIndex < poses.Length; deviceIndex++)
        {
            if (trackingReferences.ContainsKey(deviceIndex) == false)
            {
                ETrackedDeviceClass deviceClass = OpenVR.System.GetTrackedDeviceClass(deviceIndex);

                // We are interested in displaying the lighthouses (TrackingReference) and headset (HMD)
                if (deviceClass == ETrackedDeviceClass.TrackingReference || deviceClass == ETrackedDeviceClass.HMD)
                {
                    TrackingReferenceObject trackingReference = new TrackingReferenceObject();
                    trackingReference.trackedDeviceClass = deviceClass;
                    trackingReference.gameObject = new GameObject("Tracking Reference " + deviceIndex.ToString());
                    trackingReference.gameObject.transform.parent = this.transform;
                    trackingReference.trackedObject = trackingReference.gameObject.AddComponent<SteamVR_TrackedObject>();
                    trackingReference.renderModel = trackingReference.gameObject.AddComponent<SteamVR_RenderModel>();
                    trackingReference.renderModel.createComponents = false;
                    trackingReference.renderModel.updateDynamically = false;

                    trackingReferences.Add(deviceIndex, trackingReference);

                    trackingReference.gameObject.SendMessage("SetDeviceIndex", (int)deviceIndex, SendMessageOptions.DontRequireReceiver);

                    //Debug.Log($"l: { CalibrationObjectLayer.value}");
                    trackingReference.gameObject.layer = CalibrationObjectLayer;

                    // TODO: Figure out a better way to make the render models appear bigger on the valibration visual.
                    //       For now, just scale them up.
                    //
                    //       Another option could be to just intantiate our own prefab instead...
                    //       Instantiate(TrackedReference_Scene_Space_Indicator_Prefab, trackingReference.gameObject.transform);
                    if (shouldScaleRenderModels)
                    {
                        var scaleFactor = (deviceClass == ETrackedDeviceClass.HMD) ? 3 : 4;
                        trackingReference.gameObject.transform.localScale = Vector3.one * scaleFactor;
                    }
                }
                else
                {
                    trackingReferences.Add(deviceIndex, new TrackingReferenceObject() { trackedDeviceClass = deviceClass });
                }
            }
        }
    }

    private class TrackingReferenceObject
    {
        public ETrackedDeviceClass trackedDeviceClass;
        public GameObject gameObject;
        public SteamVR_RenderModel renderModel;
        public SteamVR_TrackedObject trackedObject;
    }

}
