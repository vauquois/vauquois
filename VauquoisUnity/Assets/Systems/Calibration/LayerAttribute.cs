﻿using UnityEngine;
/// <summary>
/// Attribute to select a single layer.
/// </summary>
public class LayerAttribute : PropertyAttribute
{
    // From: https://answers.unity.com/questions/609385/type-for-layer-selection.html
    // NOTHING - just oxygen.
}