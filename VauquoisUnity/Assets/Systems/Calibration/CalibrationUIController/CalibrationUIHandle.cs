﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/**
 * Super class for calibration UI handles
 */
public class CalibrationUIHandle : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{

    public Color interactableColor = Color.white;
    public Color nonInteractableColor = Color.grey;

    [Tooltip("The RectTransform which will contain this handle. Inherets from parent CalibrationUIHandle. If that's null, uses it's own parent.")]
    public RectTransform Container;

    [Tooltip("The transform to manipulate. Uses gameObject.transform if null.")]
    public RectTransform targetTransform;

    public bool interactable = true;

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (!interactable) return;
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        
    }

    private void Start()
    {
        if (Container == null)
        {
            CalibrationUIHandle parentUI = transform.parent.GetComponent<CalibrationUIHandle>();
            if (parentUI)
            {
                if (parentUI.Container != null)
                {
                    Container = parentUI.Container;
                }
                else
                {
                    Container = transform.parent.GetComponent<RectTransform>();
                }
            }
            else
            {
                Container = transform.parent.GetComponent<RectTransform>();
            }
        }

        if (targetTransform == null)
        {
            targetTransform = gameObject.GetComponent<RectTransform>();
        }
    }

    private void Update()
    {
        Image i = GetComponent<Image>();
        if (i != null)
        {
            i.color = interactable ? interactableColor : nonInteractableColor;
        }
    }
}
