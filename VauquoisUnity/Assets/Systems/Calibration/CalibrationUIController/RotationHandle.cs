﻿using UnityEngine;
using UnityEngine.EventSystems;

/**
 * Handle for the rotation widgets
 */
public class RotationHandle : CalibrationUIHandle
{
    public RectTransform showMouse;

    // TODO: Programatically calculate this point rather than assigning it
    [Tooltip("The RectTransform to rotate around. Uses targetTransform.anchoredPosition if null.")]
    public RectTransform rotateAround;

    private RectTransform Pivot
    {
        get
        {
            return (rotateAround == null) ? targetTransform : rotateAround;
        }
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (!interactable) return;

        // https://answers.unity.com/questions/892333/find-xy-cordinates-of-click-on-uiimage-new-gui-sys.html
        Vector2 localCursor;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(Container, eventData.position, eventData.pressEventCamera, out localCursor))
            return;

        float targetRotation = Mathf.Atan2((localCursor.y - Pivot.localPosition.y), (localCursor.x - Pivot.localPosition.x)) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.Euler(0, 0, targetRotation);


        if (showMouse != null) showMouse.anchoredPosition = localCursor;
        targetTransform.rotation = rot;
    }
}
