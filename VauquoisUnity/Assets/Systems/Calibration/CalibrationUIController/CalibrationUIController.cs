﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

/**
 * This class controls calibration UI.
 * 
 * 
 * // TODO: Abstract out non tunnel transforming functionality
 * // TODO: Use built in methods from RectTransformUtility to do world to rect calculations instead of using my own homemade functions 
 */
public class CalibrationUIController : MonoBehaviour
{
    [Header("UI References")]
    [SerializeField] private TextMeshProUGUI displayText;

    [SerializeField] private Image UITunnelImage;
    [SerializeField] private List<GameObject> RotationHandles; // I know this is kinda scuffed

    [SerializeField] private Button editButton;
    [SerializeField] private Button cancelButton;
    [SerializeField] private Button saveButton;
    [SerializeField] private Button autoButton;

    [Header("Settings")]
    [SerializeField, Layer] private int CalibrationObjectLayer = 0;

    //-------------------------------------------------------------

    public static Action<CalibrationState> OnStateChanged;
    public enum CalibrationState { Idle, InProgress }
    private CalibrationState mState;
    public CalibrationState State
    {
        get
        {
            return mState;
        }
        private set
        {
            if (value != mState)
            {
                mState = value;
                OnStateChanged.Invoke(mState);
            }
        }
    }

    public static Action OnSaveClicked;

    private string mLastAutoAxis; // Dont change this by hand //TODO: enforce this
    public static Action<char> OnAutoAlignButtonClicked;

    private CalibrationTunnelController mTunnel;
    private CalibrationTunnelController Tunnel
    {
        get
        {
            RegisterTunnel();
            return mTunnel;
        }
        set { mTunnel = value; }
    }

    private Camera mCalibrationRenderCamera;
    private Camera CalibrationRenderCamera
    {
        get
        {
            RegisterCamera();
            return mCalibrationRenderCamera;
        }
        set { mCalibrationRenderCamera = value; }
    }

    public Vector2 UITunnelEnd
    {
        get
        {
            Vector2 angle = UITunnelImage.rectTransform.rotation * new Vector2(UITunnelImage.rectTransform.rect.width, 0);
            Vector2 end = UITunnelImage.rectTransform.anchoredPosition + angle;
            return end;
        }
    }

    private bool shouldManipulateTunnel = false;

    // Used in get accesor above
    private void RegisterTunnel()
    {
        // Tunnel already registered
        if (mTunnel != null) return;

        // Find the tunnel in the scene
        mTunnel = FindObjectOfType<CalibrationTunnelController>();

        // Couldnt find it
        if (mTunnel == null)
        {
            Debug.LogError($"{GetType().Name} could not find a {typeof(CalibrationTunnelController).Name} to calibrate.");
        }
    }

    // Used in get accesor above
    private void RegisterCamera()
    {
        // Camera already registered
        if (mCalibrationRenderCamera != null) return;

        // Get all the cameras
        Camera[] cams = FindObjectsOfType<Camera>();

        // Find the one which whis layer is calibrationObjectLayer
        foreach (Camera c in cams)
        {
            if (c.gameObject.layer == CalibrationObjectLayer)
            {
                mCalibrationRenderCamera = c;
            }
        }

        // Couldnt find it
        if (mCalibrationRenderCamera == null)
        {
            Debug.LogError($"{GetType().Name} could not find a calibration camera in the scene.");
        }
    }

    void Start()
    {
        // Subscribe button visibility to state changes 
        // and handles interactability to state changes
        OnStateChanged += (s) =>
        {
            Debug.Log("New state: " + s);
            saveButton.gameObject.SetActive(s == CalibrationState.InProgress);
            editButton.gameObject.SetActive(s == CalibrationState.Idle);
            cancelButton.gameObject.SetActive(s == CalibrationState.InProgress);
            autoButton.gameObject.SetActive(s == CalibrationState.InProgress);

            UITunnelImage.GetComponent<CalibrationUIHandle>().interactable = s == CalibrationState.InProgress;
            RotationHandles.ForEach(handle => handle.SetActive(s == CalibrationState.InProgress));
        };

        // Flicker state change to give buttons correct visibility at start
        // TODO: Better way to do this?
        State = CalibrationState.InProgress;
        State = CalibrationState.Idle;
    }

    private void OnEnable()
    {
        CalibrationTunnelController.OnTunnelAligned += UpdateUI;        // Update our UI each time the tunnel is aligned
        CalibrationTunnelController.OnTunnelAligned += AlignUIToTunnel; // Move our UI to match the transfrom of the tunnel once it's aligned
        CalibrationBackend.OnSave += UpdateUI;                          // Update our UI anytime we save

        // Subscibe state changes to button clicks
        saveButton.onClick.AddListener(() => {
            Debug.Log("Save Clicked");
            OnSaveClicked.Invoke();
            State = CalibrationState.Idle;
        });

        editButton.onClick.AddListener(() => {
            Debug.Log("Edit Clicked");
            State = CalibrationState.InProgress;
        });

        cancelButton.onClick.AddListener(() => {
            Debug.Log("Cancel Clicked");
            Tunnel.AlignToCurrentSave();
            State = CalibrationState.Idle;
        });
         
        autoButton.onClick.AddListener(() =>
        {
            mLastAutoAxis = mLastAutoAxis == "a" ? "b" : "a";
            OnAutoAlignButtonClicked.Invoke(mLastAutoAxis.ToCharArray()[0]);
        });
    }

    // Dont worry this is called on OnDestory as well
    private void OnDisable()
    {
        // Go back to idle state, disregarding any changes if the menu is closed during editing
        // TODO: Investigate null reference error when going back to landing scene. Caused by Camera.main being null
        Tunnel.AlignToCurrentSave();
        State = CalibrationState.Idle;

        // Unsub from other scripts actions
        CalibrationTunnelController.OnTunnelAligned -= UpdateUI;
        CalibrationTunnelController.OnTunnelAligned -= AlignUIToTunnel;
        CalibrationBackend.OnSave -= UpdateUI;

        // Unsub button events
        saveButton.onClick.RemoveAllListeners();
        editButton.onClick.RemoveAllListeners();
        cancelButton.onClick.RemoveAllListeners();
        autoButton.onClick.RemoveAllListeners();
    }

    private void UpdateUI()
    {
        CalibrationBackend.CalibrationData currentData = CalibrationBackend.CurrentData;
        if (currentData == null) return;
        updateText(currentData.Date);
    }

    void Update()
    {
        if (shouldManipulateTunnel)
        {
            AlignTunnelToUI();
        }
    }

    // Call continuously to align the real tunnel with the UI view
    private void AlignTunnelToUI()
    {
        // Convert the position of the beginning and end of the UI Tunnel to world space
        Vector3 worldPointStart = NormalizedSpaceToWorldSpace(NormalizedPosition(UITunnelImage.rectTransform), CalibrationRenderCamera, 0);
        Vector3 worldPointEnd = NormalizedSpaceToWorldSpace(NormalizedPosition(UITunnelImage.rectTransform, UITunnelEnd), CalibrationRenderCamera, 0);

        // Align the tunnel to the start position and direction of (start - end)
        Tunnel.TunnelStart = worldPointStart;
        Tunnel.Direction = worldPointEnd - worldPointStart;
    }

    // TODO: There is likely a better fast way to do this. This will do for now though
    private void AlignUIToTunnel()
    {
        // Set the UI tunnels length and position to match the one in the scene
        float normalizedLength = Vector3.Distance(WorldSpaceToNormalizedSpace(Tunnel.TunnelEnd), WorldSpaceToNormalizedSpace(Tunnel.TunnelStart));
        UITunnelImage.rectTransform.sizeDelta = new Vector2(normalizedLength * UITunnelImage.rectTransform.parent.GetComponent<RectTransform>().rect.width, UITunnelImage.rectTransform.sizeDelta.y); // TODO: Make a function which does this
        UITunnelImage.rectTransform.anchoredPosition = RawPosition(UITunnelImage.rectTransform, WorldSpaceToNormalizedSpace(Tunnel.TunnelStart));

        // Set the UI tunnels rotation to match the one in the scene
        // Help from: https://stackoverflow.com/questions/41492962/how-to-rotate-unity-ui-recttransform-so-that-it-points-at-another-ui-element
        Vector3 tunnelEndInViewport = RawPosition(UITunnelImage.rectTransform, WorldSpaceToNormalizedSpace(Tunnel.TunnelEnd));
        float targetRotation = Mathf.Atan2((tunnelEndInViewport.y - UITunnelImage.rectTransform.anchoredPosition.y), (tunnelEndInViewport.x - UITunnelImage.rectTransform.anchoredPosition.x));
        UITunnelImage.rectTransform.rotation = Quaternion.Euler(0, 0, targetRotation* Mathf.Rad2Deg);
        
        // Start manipulating the real tunnel
        // TODO: Investigate how this might effect other attempts
        //       to move the tunnel, given that this variable being true will
        //       allow this script to change the transform of the tunnel every update loop.
        shouldManipulateTunnel = true;
    }

    // Convert a point (from the world) to normalized calibration space coordinates (0, 1)
    private Vector3 WorldSpaceToNormalizedSpace(Vector3 point)
    {
        Vector3 cameraSpacePoint = CalibrationRenderCamera.WorldToViewportPoint(point);
        return cameraSpacePoint;
    }

    // Convert pos (which should be normalized using NormalizedPosition) to worldspace from cam's point of view at y position y.
    private Vector3 NormalizedSpaceToWorldSpace(Vector3 pos, Camera cam, float y)
    {
        Vector3 posReal = cam.ViewportToWorldPoint(pos);
        return new Vector3(posReal.x, y, posReal.z);
    }

    // Convert the localposition of this rect transform to a value such that 
    // it represents its distance from the bottom left corner of its parent using values between 0 and 1.
    // Optionally, convert pos instead, but use rt's parent.
    //    1
    //     |
    //     |
    //     |
    //    0|__________
    //     0          1
    //
    // Note: This method depends on the anchor of rt to be at the bottom left corner of its parent.
    // TODO: There is likely a better fast way to do this. This will do for now though
    private Vector3 NormalizedPosition(RectTransform rt, Vector2? pos = null)
    {
        return new Vector3((pos?.x ?? rt.anchoredPosition.x) / rt.parent.GetComponent<RectTransform>().rect.width,
                           (pos?.y ?? rt.anchoredPosition.y) / rt.parent.GetComponent<RectTransform>().rect.height, 
                           0);
    }

    // Inverse of NormalizedPosition.
    // Optionally, convert pos instead, but use rt's parent.
    // TODO: There is likely a better fast way to do this. This will do for now though
    private Vector3 RawPosition(RectTransform rt, Vector2? pos = null)
    {
        return new Vector3((pos?.x ?? rt.anchoredPosition.x) * rt.parent.GetComponent<RectTransform>().rect.width,
                           (pos?.y ?? rt.anchoredPosition.y) * rt.parent.GetComponent<RectTransform>().rect.height,
                           0);
    }

    // Update the text in the calibration window
    // TODO: Make this Last Calibration: 1 min ago ||  50 seconds ago || 2/28/20
    public void updateText(string s)
    {
        string info = string.Empty;
        string date = s;

        if (date == "")
        {
            info = "No calibration data saved.";

        }
        else
        {
            info = "Last Saved: " + date;
        }

        displayText.text = info;
    }
}
