﻿using UnityEngine;
using UnityEngine.EventSystems;

/**
 * Handle for the viewport tunnel
 */
public class TunnelHandle : CalibrationUIHandle
{
    private Vector3 offset;

    public override void OnBeginDrag(PointerEventData eventData)
    {
        offset = new Vector2(transform.position.x, transform.position.y) - eventData.position;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (!interactable) return;

        if (RectTransformUtility.RectangleContainsScreenPoint(Container, eventData.position))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                transform.Rotate(Vector3.forward, -eventData.delta.x);
            }
            else
            {
                transform.position = new Vector3(eventData.position.x, eventData.position.y) + offset;
            }
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        offset = Vector3.zero;
    }
}
