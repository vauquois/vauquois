﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTimeCalibrationTip : MonoBehaviour
{
    [SerializeField] private GameObject visual;
    void Start()
    {
        UpdateVisual();
        CalibrationBackend.OnSave += UpdateVisual;
    }

    private void UpdateVisual()
    {
        visual.SetActive(!VauquoisSettings.hasCompletedFirstCalibration);
    }

    private void OnDestroy()
    {
        CalibrationBackend.OnSave -= UpdateVisual;
    }
}
