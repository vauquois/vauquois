﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/**
 * Provides autoscroll functionality to the credits.
 * Provides a public boolean toggle which can be used by toggles in the scene to enable and disable autoscrolling.
 * Derived from: https://answers.unity.com/questions/1432377/scroll-rect-autoscroll-content.html
 */
public class CreditsUtility : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField, Range(0f, 1f)]
    private float scrollSpeed = 2f;
    [SerializeField, Range(1f, 5f)]
    private float watingTime = 5f;

    private float maxScroll;
    private ScrollRect scrollRect;
    private RectTransform contenRectTransform;
    private Vector2 defaultPosition;
    private bool canMove;

    //---------------------------------------------------------------------

    private void Start()
    {
        // TODO: Move this to onEnable so that autoscrolling doesnt ahve to be "refreshed" to take effect
        this.scrollRect = GetComponentInChildren<ScrollRect>();
        this.contenRectTransform = this.scrollRect.content;
        this.maxScroll = this.contenRectTransform.rect.yMax;
        this.StartCoroutine(this.ActivateAutoMove());
    }
    private void Update()
    {
        bool hasScrolled = this.contenRectTransform.position.y < this.maxScroll;
        if (canMove & !hasScrolled)
        {
            this.Move();
        }
    }
    private void Move()
    {
        // TODO: Add easing
        Vector3 contentPosition = this.contenRectTransform.position;
        float newPositionY = contentPosition.y + this.scrollSpeed;
        Vector3 newPosition = new Vector3(contentPosition.x, newPositionY, contentPosition.z);
        this.contenRectTransform.position = newPosition;
    }

    private IEnumerator ActivateAutoMove()
    {
        yield return new WaitForSeconds(watingTime);
        canMove = true;
    }

    //---------------------------------------------------------------------

    public void ToggleScrolling(bool b)
    {
        StopAllCoroutines();
        canMove = b;
    }

    public void ResetToTop()
    {
        canMove = false;
        scrollRect.normalizedPosition = Vector2.one;
    }
}
