﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using static AdaptiveLanternController;

/**
 * This script will usually be attached to a Gameobject which is the parent of a number of Toggles
 * representing the options the user has for lantern settings. It must always (and can only) be used with a ToggleGroup.
 * 
 * During Start, OnEnable, and after any click on a toggle, the toggles will be updated to match the setting found in VauquoisSettings.lanternType.
 * When a toggle is clicked, if it has been assigned correctly to correspond to a lantern type, VauquoisSettings will be updated and written to disk.\
 * 
 * There is editor validation to help developers set up the toggleLanternTypePairs correctly.
 * 
 * Author: Dillon Cutaiar (dillonc@vt.edu)
 */
[RequireComponent(typeof(ToggleGroup))]
public class LanternTypeSetting : MonoBehaviour
{

    // Allow developers to assign Toggle-LanternType pairs in the inspector, indicating expected bevahiour of this settings menu.
    [Serializable]
    public class ToggleLanternTypePair
    {
        public Toggle toggle;
        public LanternType lanternType;
    }
    [Header("Assignments")]
    [SerializeField] private List<ToggleLanternTypePair> toggleLanternTypePairs;

    private ToggleGroup toggleGroup;
    private Toggle lastToggle = null;

    private void Start()
    {
        toggleGroup = GetComponent<ToggleGroup>();

        // There must always be one toggle on
        toggleGroup.allowSwitchOff = false;

        // Update our view first
        UpdateUIToMatchVauquoisSettings();

        // Listen for a change in the toggle that is currently isOn
        foreach (ToggleLanternTypePair p in toggleLanternTypePairs)
        {
            p.toggle.onValueChanged.AddListener((b) => { if (b && p.toggle != lastToggle) { ActiveToggleChanged(p.toggle); lastToggle = p.toggle; }; });
        }
    }

    private void OnDestroy()
    {
        foreach (ToggleLanternTypePair p in toggleLanternTypePairs)
        {
            p.toggle.onValueChanged.RemoveAllListeners();
        }
    }

    private void OnEnable()
    {
       UpdateUIToMatchVauquoisSettings();
    }

    private void ActiveToggleChanged(Toggle newActiveToggle)
    {
        ToggleLanternTypePair pair = toggleLanternTypePairs.Find(x => x.toggle == newActiveToggle);

        if (pair == null)
        {
            Debug.LogError($"<b>[{GetType()}]</b>: Error updating lantern settings. Are your inspector assignments correct?");
        }

        // Update global settings
        VauquoisSettings.lanternType = pair.lanternType;

        // Make sure we reflect those global settings in UI (though, this should already be the case)
        UpdateUIToMatchVauquoisSettings();

        // Write to disk
        VauquoisSettings.Save();
    }

    private void UpdateUIToMatchVauquoisSettings()
    {
        // Get the toggle associated with the current Vauquois Settings
        Toggle toggle = toggleLanternTypePairs.Find(x => x.lanternType == VauquoisSettings.lanternType)?.toggle;
        if (!toggle)
        {
            Debug.LogError($"<b>[{GetType()}]</b>: Could not find toggle corresponding to: {VauquoisSettings.lanternType}. Are your inspector assignments correct?");
        }

        toggle.SetIsOnWithoutNotify(true); // Toggle group will disable all other toggles when this happens
    }

    #region EDITOR ONLY
    private void OnValidate()
    {
        int numberOfOptions = Enum.GetNames(typeof(LanternType)).Length;
        if (toggleLanternTypePairs.Count != numberOfOptions)
        {
            Debug.LogWarning($"<b>[{GetType()}] </b>: There are not enough Toggles assigned for the number of settings available.");
        }

        // TODO: Make this extendable to any number of lanternTypes (not necessary right now)
        var fail = false;
        var potentialDuplicate = toggleLanternTypePairs.Where(p => p.lanternType == LanternType.Controller);
        if (potentialDuplicate.Count() > 1) fail = true;
        potentialDuplicate = toggleLanternTypePairs.Where(p => p.lanternType == LanternType.Tracker);
        if (potentialDuplicate.Count() > 1) fail = true;

        if (fail)
        {
            Debug.LogError($"<b>[{GetType()}]</b>: There is more than one Toggle corresponding to a single Lantern type.");
        }
    }
    #endregion
}


