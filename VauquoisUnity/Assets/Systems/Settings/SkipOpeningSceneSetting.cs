﻿using UnityEngine;
using UnityEngine.UI;

/**
 * This script will control the skipOpeningScene setting using a toggle
 * 
 */
[RequireComponent(typeof(Toggle))]
public class SkipOpeningSceneSetting : MonoBehaviour
{

    private Toggle _toggle;
    private Toggle Toggle
    {
        get
        {
            if (_toggle == null)
            {
                _toggle = GetComponent<Toggle>();
            }
            return _toggle;
        }
        set => _toggle = value;
    }

    private void Start()
    {
        // Update our view first
        UpdateUIToMatchVauquoisSettings();

        // Listen for toggle events
        Toggle.onValueChanged.AddListener(ToggleChanged);
    }

    private void OnDestroy()
    {
        Toggle.onValueChanged.RemoveAllListeners();
    }

    private void OnEnable()
    {
        UpdateUIToMatchVauquoisSettings();
    }

    private void UpdateUIToMatchVauquoisSettings()
    {
        Toggle.SetIsOnWithoutNotify(VauquoisSettings.skipOpeningScene);
    }

    private void ToggleChanged(bool newValue)
    {
        // Set backend value
        VauquoisSettings.skipOpeningScene = newValue;

        // Make sure we reflect those global settings in UI (though, this should already be the case)
        UpdateUIToMatchVauquoisSettings();

        // Write to disk
        VauquoisSettings.Save();
    }
}
