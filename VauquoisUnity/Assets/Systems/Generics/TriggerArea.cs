﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class TriggerArea : MonoBehaviour
{
    protected bool bTriggered = false;
    protected string headColliderTag = "HeadCollider"; //TODO: Read this from central location

    public UnityEvent OnTriggerAreaEntered;
    void OnTriggerEnter(Collider collider)
    {
        if (Time.time > 0.1f)
        {
            if (collider.gameObject.tag == headColliderTag)
            {
                if (!bTriggered)
                {
                    OnTriggerAreaEntered?.Invoke();
                    Triggered();
                    bTriggered = true;
                }
            }
        }
    }

    protected virtual void Triggered()
    {
        Debug.Log("TriggerArea Entered");
    }
}
