﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    [SerializeField]
    private string openingScene;

    [SerializeField]
    private string landingScene;

    [SerializeField]
    private string tunnelScene;

    public void LoadOpeningScene()
    {
        SceneManager.LoadSceneAsync(openingScene, LoadSceneMode.Single);
    }

    public void LoadLandingScene()
    {
        SceneManager.LoadSceneAsync(landingScene, LoadSceneMode.Single);
    }

    public void LoadTunnelScene()
    {
        SceneManager.LoadSceneAsync(tunnelScene, LoadSceneMode.Single);
    }
}
