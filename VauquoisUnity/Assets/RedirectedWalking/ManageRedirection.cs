using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageRedirection : MonoBehaviour {

    ////////////////////////////////Data that needs to be explicitly set by the user of this modular code/////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //public GameObject[] redirectionInterfaces; //the conceptual game object that contains interface to the information needed to complete redirection (lights, collision zones, etc.)
    public GameObject hmd;   //Assigned to the one and only HMD camera gameobject (its position and orientation have to be driven by the real time motion tracking data)
    public GameObject VIVESystem;  //parent object of the HMD and controllers, used to add offset to the motion tracking system

    public RedirectionInterface[] redirectionInterfaces_scripts;
    public int starting_redir_ID = 0;  //the ID of the starting segment

    public float light_change_period = 0.1f;
    public float redirection_period = 0.1f;

    public float light_max_intensity = 1.0f;
    public float light_min_intensity = 0.0f;
    public float light_change_lasting_time = 2.0f;

	// the time after the light switch appears before the lights come up
	public float lightSwitchTimer = 0;

	public bool showLightSwitches = true;

	public ImageController_RedirectionZone corner01ImageController;
	public ImageController_RedirectionZone corner02ImageController;

    public CaveInManager caveInManager;


    ///////////////////////////////Current reference to objects needed for redirection///////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //public GameObject redirectionZone;  //the gameObject that contains the collider of the redirection zone (the "elbow" area, where rotation gain should be added)
    //public GameObject startingZone; //the gameObject that contains the collider of zone before entering the redirection zone
    //public GameObject endingZone; //the gameObject that contains the collider of zone after exiting the redirection zone

    private Light[] startingZoneLights;  //the lights in the starting zone, which should be dimmed after the user enters the redirection zone
    private Light[] redirectionZoneLights;  //the lights in the redirection zone
    private Light[] endingZoneLights;    //the lights in the ending zone, which starts initially dark and should be lightened up after user pushes the button


    private bool clockWiseRot = false;
    private GameObject redirection_pivot;

    //public GameObject button_group;
    private GameObject button;     //The button, it should contain the collider to detect button activation
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////data used to calculate the rotation gain/////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //real-time data of the HMD gameobject, read only
    private Transform hmd_local_transform_cur;
    private Quaternion hmd_local_quaternion_cur;
    private Transform hmd_local_transform_prev;
    private Quaternion hmd_local_quaternion_prev;

    //data to calculate angular difference
    private Vector3 hmd_local_forward_cur;
    private Vector3 hmd_local_forward_cur_XZ;
    private Vector3 hmd_local_forward_prev;
    private Vector3 hmd_local_forward_prev_XZ;
    private float angularDiff_XZ;

    //data to calculate positional difference
    private Vector3 hmd_local_pos_cur;
    private Vector3 hmd_local_pos_prev;
    private Vector3 hmd_world_pos_cur;
    private float hmd_local_pos_diff;

    //accumulate the rotation gain
    private float rotatedAngle = 0.0f;
    private bool hasStartedRotationGain = false;

    //current redirection area ID
    private int redirID;
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //control lighting
    //public Light firstRoomLight;
    //public Light secondRoomLight;
    private float basedLineRotationAngle = 0.0f;   //constant rotation gain. Based on my experience, having a constant rotation gain breaks the illusion so it's default to 0
                                                  //The reason it's here is that there was a constant rotation gain in the original RDW paper
                                                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private int stage = 0;


    //public bool buttonActivation = false;

    private void OnEnable()
    {
        //set correct ID
        redirID = starting_redir_ID;

        //set references
        //hmd = redirectionInterfaces_scripts[starting_redir_ID].hmd;
        //VIVESystem = redirectionInterfaces_scripts[starting_redir_ID].VIVESystem;

        //redirectionZone = redirectionInterfaces_scripts[starting_redir_ID].redirectionZone;
        //startingZone = redirectionInterfaces_scripts[starting_redir_ID].startingZone;
        //endingZone = redirectionInterfaces_scripts[starting_redir_ID].endingZone;

        //lights
        RedirectionInterface currInterface = redirectionInterfaces_scripts[starting_redir_ID];
        startingZoneLights = currInterface.startingZoneLights;
        redirectionZoneLights = currInterface.redirectionZoneLights;
        endingZoneLights = currInterface.endingZoneLights;

        //baseline rotation gain
        basedLineRotationAngle = currInterface.basedLineRotationAngle;

        //button
        //button_group = redirectionInterfaces_scripts[starting_redir_ID].button_group;
        button = currInterface.button;

        redirection_pivot = currInterface.redirection_pivot;
        clockWiseRot = currInterface.clockwiseRot;

        //set initial data
        enterNewStage(0);
    }

    // Use this for initialization
    void Start () {
		/*
		for (int i = 0; i < redirectionInterfaces_scripts.Length; i++) {
			if (i > 0) {
				startingZoneLights = redirectionInterfaces_scripts [starting_redir_ID].startingZoneLights;
				for (int j = 0; j < startingZoneLights.Length; j++) {
					startingZoneLights[j].intensity = 0;
				}
				redirectionZoneLights = redirectionInterfaces_scripts[starting_redir_ID].redirectionZoneLights;
				for (int j = 0; j < redirectionZoneLights.Length; j++) {
					redirectionZoneLights[j].intensity = 0;
				}
				endingZoneLights = redirectionInterfaces_scripts[starting_redir_ID].endingZoneLights;
				for (int j = 0; j < endingZoneLights.Length; j++) {
					endingZoneLights[j].intensity = 0;
				}
			}
		}
		*/
        //rotatedAngle = 0.0f;
        //button = button_group.gameObject.transform.Find("button").gameObject;
	} 
	
	// Update is called once per frame
	void Update () {
		if (stage == 2) {
			lightSwitchTimer += Time.deltaTime;
			//if (lightSwitchTimer > 5) {
				enterNewStage (3);
				lightSwitchTimer = 0;
			//}
		}

        /*
        //angular
        hmd_local_transform_cur = hmd.transform;
        hmd_local_quaternion_cur = hmd_local_transform_cur.localRotation;
        hmd_local_forward_cur = hmd_local_quaternion_cur * Vector3.forward;
        hmd_local_forward_cur_XZ = hmd_local_forward_cur;
        hmd_local_forward_cur_XZ.y = 0.0f; //current XZ local forward vector
        angularDiff_XZ = Vector3.Angle(hmd_local_forward_prev_XZ, hmd_local_forward_cur_XZ);

        //positional
        hmd_local_pos_cur = hmd_local_transform_cur.localPosition;
        hmd_local_pos_diff = Vector3.Distance(hmd_local_pos_cur, hmd_local_pos_prev);

        hmd_world_pos_cur = hmd_local_transform_cur.position;
        //redirection
        if (redirectionZone.GetComponent<Collider>().bounds.Contains(hmd_world_pos_cur) && rotatedAngle < 90.0f && stage == 0)
        {
            float rotationGainBasedOnHeadRotation = 0.0f;
            rotationGainBasedOnHeadRotation = angularDiff_XZ * 0.1f;

            float actualRotationGain = Mathf.Max(rotationGainBasedOnHeadRotation, basedLineRotationAngle);
            VIVESystem.transform.Rotate(Vector3.up, -actualRotationGain);
            rotatedAngle += actualRotationGain;

            if (firstRoomLight.intensity > 0.0f)
                firstRoomLight.intensity -= (0.2f * Time.deltaTime);
        }
        //firstRoomLight.intensity = 1.0f * (1 - rotatedAngle / 90.0f);
        //secondRoomLight.intensity = 1.0f * rotatedAngle / 90.0f;

        //if (secondRoomLight.intensity < 1.0f && rotatedAngle >= 89.0f) //reached the threshold
        //    secondRoomLight.intensity += (0.2f * Time.deltaTime);

        //set button active
        if (rotatedAngle >= 89.0f && stage == 0)
        {
            button_group.SetActive(true);
            stage = 1;
        }

        //on collision with button and 
        //secondRoomLight.intensity += (0.2f * Time.deltaTime);
        if (buttonActivation && secondRoomLight.intensity < 1.0f)
            secondRoomLight.intensity += (0.2f * Time.deltaTime);

        //original
        //if (secondRoomLight.intensity < 1.0f && rotatedAngle >= 89.0f) //reached the threshold
        //    secondRoomLight.intensity += (0.2f * Time.deltaTime);

        //update for "previous" data afterwards
        hmd_local_transform_prev = hmd_local_transform_cur;

        //angular
        hmd_local_quaternion_prev = hmd_local_quaternion_cur;
        hmd_local_forward_prev = hmd_local_forward_cur;
        hmd_local_forward_prev_XZ = hmd_local_forward_cur_XZ;

        //positional
        hmd_local_pos_prev = hmd_local_pos_cur;
        */
    }

    public void enterNewStage (int newStage)
    {
        switch (newStage)
        {
			// when entering stage 0, light up the initial starting area and the first redirection zone
            case 0: 
                {
                    rotatedAngle = 0.0f;
                    hasStartedRotationGain = false;
                    stage = 0;
                    StartCoroutine(changeLightsIntensity(light_max_intensity, startingZoneLights, light_change_lasting_time));  //lights up the starting zone
                    StartCoroutine(changeLightsIntensity(light_max_intensity, redirectionZoneLights, light_change_lasting_time)); //lights up the redirection zone
                    break;
                }              
			// stage 1 is entered when we enter a redirection zone.
			// when entering stage 1, start appllying rotation gain and dim the lights behind you
            case 1: 
                {
                    if (stage != 0)
                        break;
                    stage = 1;
                    StartCoroutine(applyRotationGain()); //start applying rotation gain
                    StartCoroutine(changeLightsIntensity(light_min_intensity, startingZoneLights, light_change_lasting_time)); //dim starting zone lighting
                    break;
                }     
			// stage 2 is entered as soon as redirection is done
            case 2:
                {
                    if (stage != 1)
                        break;
                    stage = 2;
					if (showLightSwitches) {
						//show button
						//button.SetActive (true);
					}
                    break;
                }
			// stage 3 may be entered immediately after stage 2 is over, or it may allow a few seconds for the user to find the light switch
			// this is controller above, in update, in an if statement
            case 3:
                {
                    if (stage != 2)
                        break;
                    stage = 3;
                    StartCoroutine(changeLightsIntensity(light_max_intensity, endingZoneLights, light_change_lasting_time));
					corner01ImageController.FadeOutImages ();

                    if (caveInManager != null)
                    {
                        Debug.Log("Triggering caveInManager cave in");
                        caveInManager.TriggerCaveIn();
                    }
                    else
                    {
                        Debug.LogWarning("RedirectionManager tried to trigger a cave in, but is missing a reference to caveInManager. Is this intentional?");
                    }

                    // These corner image controllers are clearly shoehorned in, so I added a quick null check to fix null references for now.
                    if (corner02ImageController == null) Debug.LogWarning("RedirectionManager is missing a reference to corner02ImageController. Is this intentional?");
                    else corner02ImageController.FadeOutImages();
                    break;
                }
            case 4:
                {
                    if (stage != 3)
                        break;
                    stage = 4;
                    StartCoroutine(changeLightsIntensity(light_min_intensity, redirectionZoneLights, light_change_lasting_time));
                    break;
                }
            default:
                {
                    Debug.Log("wrong state input");
                    break;
                }               
        }
    }

    public void enterNewRedirArea(int ID)
    {
        redirID = ID;
        //find new references
        startingZoneLights = redirectionInterfaces_scripts[redirID].startingZoneLights;
        redirectionZoneLights = redirectionInterfaces_scripts[redirID].redirectionZoneLights;
        endingZoneLights = redirectionInterfaces_scripts[redirID].endingZoneLights;

        basedLineRotationAngle = redirectionInterfaces_scripts[redirID].basedLineRotationAngle;

        //button_group = redirectionInterfaces_scripts[starting_redir_ID].button_group;
        button = redirectionInterfaces_scripts[redirID].button;

        clockWiseRot = redirectionInterfaces_scripts[redirID].clockwiseRot;
        redirection_pivot = redirectionInterfaces_scripts[redirID].redirection_pivot;

        //set initial data
        rotatedAngle = 0.0f;

        enterNewStage(0);
    }

    public void enterRedirectionZone (int zone_area_ID)
    {
		Debug.Log ("enter redirection zone.. zone_area_ID: " + zone_area_ID + " redirID: " + redirID);
        if (zone_area_ID == redirID)   //if colliding happens in the same redirection area
        {
            if (stage == 0)
                enterNewStage(1);
            else
                Debug.Log("wanted to enter stage 1 but couldn't because current stage was not 0");
        }
        else
            Debug.Log("entered a redirection zone but not the one in the current redirection area");
    }

    public void enterExitZone (int zone_area_ID)
    {
        if (zone_area_ID == redirID)   //if colliding happens in the same redirection area
        {
            if (stage == 3)
            {
                enterNewStage(4);
                if (redirID + 1 < redirectionInterfaces_scripts.Length)
                    enterNewRedirArea(redirID + 1);
            }
            else
                Debug.Log("wanted to enter stage 1 but couldn't because current stage was not 0");
        }
        else
            Debug.Log("entered a redirection zone but not the one in the current redirection area");
    }

    public void buttonPressed (int zone_area_ID)
    {
        if (zone_area_ID == redirID)  //if button of the current redirection area is pressed
        {
            if (stage == 2)
                enterNewStage(3);
            else
                Debug.Log("wanted to enter stage 3 but couldn't because current stage was not 2");
        }
        else
            Debug.Log("button pressed but not the button of the current redirection area");
    }

    IEnumerator changeLightsIntensity (float targetIntensity, Light[] lights, float timeToReachTarget)
    {
        // Simple check to ignore this function if an empty or null array of lights is passed in
        if (lights == null || lights.Length == 0 || Array.Exists<Light>(lights, (element) => element == null))
        {
            Debug.LogWarning("Redirection Manager is attempting to change the intensity of non existent lights. Are you missing a reference in the editor?");
            yield break;
        }

        float timeLeft = timeToReachTarget;
        int lightsCount = lights.Length;
        
        float[] lightsChangeSpeeds = null;
        if (lightsCount > 0)
            lightsChangeSpeeds = new float[lightsCount];
        for (int i= 0; i < lightsCount; i++)
            lightsChangeSpeeds[i] = (targetIntensity - lights[i].intensity) / timeToReachTarget * light_change_period;

        while (timeLeft > 0.0f)
        {
            //bool alreadyReachedTarget = true;
            for (int i=0; i < lightsCount; i++)
            {
                float newIntensity = lights[i].intensity + lightsChangeSpeeds[i];
                if (newIntensity > 0.0f)
                    lights[i].intensity = newIntensity;
                else
                    lights[i].intensity = 0.0f;
            }
            timeLeft -= light_change_period;
            yield return new WaitForSeconds(light_change_period);
        }
    }

    IEnumerator applyRotationGain ()
    {
        while (rotatedAngle < 92.0f)
        { 
            if (!hasStartedRotationGain) //fresh start
            {
                hmd_local_transform_prev = hmd.transform;

                //angular
                hmd_local_quaternion_prev = hmd_local_transform_prev.localRotation;
                hmd_local_forward_prev = hmd_local_quaternion_prev * Vector3.forward;
                hmd_local_forward_prev_XZ = hmd_local_forward_prev;
                hmd_local_forward_prev_XZ.y = 0.0f;

                //positional
                hmd_local_pos_prev = hmd_local_transform_prev.position;
                hasStartedRotationGain = true;
                yield return null;
            }
            //angular
            hmd_local_transform_cur = hmd.transform;
            hmd_local_quaternion_cur = hmd_local_transform_cur.localRotation;
            hmd_local_forward_cur = hmd_local_quaternion_cur * Vector3.forward;
            hmd_local_forward_cur_XZ = hmd_local_forward_cur;
            hmd_local_forward_cur_XZ.y = 0.0f; //current XZ local forward vector
            angularDiff_XZ = Vector3.Angle(hmd_local_forward_prev_XZ, hmd_local_forward_cur_XZ);

            //positional
            hmd_local_pos_cur = hmd_local_transform_cur.localPosition;
            hmd_local_pos_diff = Vector3.Distance(hmd_local_pos_cur, hmd_local_pos_prev);

            hmd_world_pos_cur = hmd_local_transform_cur.position;
    
            float rotationGainBasedOnHeadRotation = 0.0f;
            rotationGainBasedOnHeadRotation = angularDiff_XZ * 0.1f;

            float actualRotationGain = Mathf.Max(rotationGainBasedOnHeadRotation, basedLineRotationAngle);
			//if (actualRotationGain > 0.25f) {
			//	actualRotationGain = 0.02f;
			//}

            //VIVESystem.transform.Rotate(Vector3.up, -actualRotationGain); //to be done. rotate around
            if (clockWiseRot)
                VIVESystem.transform.RotateAround(redirection_pivot.transform.position, Vector3.up, actualRotationGain);
            else
                VIVESystem.transform.RotateAround(redirection_pivot.transform.position, Vector3.up, -actualRotationGain);

            rotatedAngle += actualRotationGain;

            //update for "previous" data afterwards
            hmd_local_transform_prev = hmd_local_transform_cur;

            //angular
            hmd_local_quaternion_prev = hmd_local_quaternion_cur;
            hmd_local_forward_prev = hmd_local_forward_cur;
            hmd_local_forward_prev_XZ = hmd_local_forward_cur_XZ;

            //positional
            hmd_local_pos_prev = hmd_local_pos_cur;

            //yield return new WaitForSeconds(redirection_period);
            yield return null;
        }
        //enter next phase
        enterNewStage(2);
    }
}
